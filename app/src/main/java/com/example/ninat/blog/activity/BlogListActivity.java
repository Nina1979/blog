/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.example.ninat.blog.activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ninat.blog.R;
import com.example.ninat.blog.adapter.ListAdapter;
import com.example.ninat.blog.model.Blog;
import com.example.ninat.blog.model.BlogContent;
import com.example.ninat.blog.model.Token;
import com.example.ninat.blog.rest.ApiClient;
import com.example.ninat.blog.rest.CreitiveAPI;

import com.orm.SugarRecord;
import com.orm.query.Select;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ninat on 25/03/2017.
 * Displays list of blogs on the screen
 */

public class BlogListActivity extends AppCompatActivity {
    private ListAdapter mAdapter;
    @BindView(R.id.blogList) ListView lvBlogList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_list);
        ButterKnife.bind(this);

        //Checks if internet is turned on and if it isnt toasts the message to the user.
        if (!isNetworkAvailable()) {
            Toast.makeText(BlogListActivity.this, "Turn your Internet connection on!", Toast.LENGTH_LONG).show();
            return;
        }

        //Gets cached date when list of blogs were saved in SharedPreference
        Long mCacheDate = PreferenceManager.getDefaultSharedPreferences(BlogListActivity.this)
                .getLong("DateOfSavingBlogListToDatabase", 0);

        //Checks if cache exists and if its not expired.If true it loads list of blogs, creates an insatnce of mAdapter and sets this mAdapter on listview
        if (mCacheDate != 0 && System.currentTimeMillis() - 7200000L <= mCacheDate) {
            List<Blog> mBlogs = Select.from(Blog.class).list();
            if (!mBlogs.isEmpty()) {
                mAdapter = new ListAdapter(BlogListActivity.this, mBlogs);
                lvBlogList.setAdapter(mAdapter);
                return;
            }
        }

        // Sends new request to server and returns list of blogs
        fetchBlogList();
    }


    private void fetchBlogList() {
        //takes token
        Token mToken = ApiClient.token;

        //Creates an instance of HashMap for  saving a dynamic header which will be later used for post request
        Map<String, String> mHeaders = new HashMap<>();
        mHeaders.put("X-Client-Platform", "Android");
        mHeaders.put("Accept", "application/json");
        mHeaders.put("Content-Type", "application/json");
        mHeaders.put("X-Authorize", mToken.getsToken());

        //Sends a call to a server
        CreitiveAPI api = ApiClient.getClient().create(CreitiveAPI.class);
        Call<List<Blog>> call = api.getPosts(mHeaders);
        call.enqueue(new Callback<List<Blog>>() {
            @Override
            public void onResponse(Call<List<Blog>> call, Response<List<Blog>> response) {
                switch (response.code()) {
                    case 200:
                        //Deletes all previous cached records of blogs and blog contents
                        SugarRecord.deleteAll(Blog.class);
                        SugarRecord.deleteAll(BlogContent.class);

                        List<Blog> blogs = response.body();
                        //Saves blogs to database
                        for (Blog b : blogs) {
                            SugarRecord.save(b);
                        }
                        ;
                        //Saves exact time when blogs and content blogs records were saved into database
                        Date date = new Date(System.currentTimeMillis());
                        PreferenceManager.getDefaultSharedPreferences(BlogListActivity.this)
                                .edit()
                                .putLong("DateOfSavingBlogListToDatabase", date.getTime())
                                .apply();
                        // Creates an instance of an mAdapter and sets it listview
                        mAdapter = new ListAdapter(BlogListActivity.this, blogs);
                        lvBlogList.setAdapter(mAdapter);
                        break;
                    case 401:
                        Toast.makeText(BlogListActivity.this, "Token is missing!", Toast.LENGTH_LONG).show();
                        break;
                    case 406:
                        Toast.makeText(BlogListActivity.this, "System error!", Toast.LENGTH_LONG).show();
                        break;
                    case 415:
                        Toast.makeText(BlogListActivity.this, "System error!", Toast.LENGTH_LONG).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<Blog>> call, Throwable t) {
                Toast.makeText(BlogListActivity.this, "System error!", Toast.LENGTH_LONG).show();
            }
        });
    }


    //Checks if the internet is turned on
    private boolean isNetworkAvailable() {
        ConnectivityManager mConnectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}



