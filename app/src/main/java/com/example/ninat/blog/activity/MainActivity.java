/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.ninat.blog.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ninat.blog.R;
import com.example.ninat.blog.model.Data;
import com.example.ninat.blog.model.Token;
import com.example.ninat.blog.rest.ApiClient;
import com.example.ninat.blog.rest.CreitiveAPI;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ninat on 25/03/2017.
 * Displays login screen
 */

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.editEmail) EditText email;
    @BindView(R.id.editPassword) EditText password;
    @BindView(R.id.btnLogin) Button login;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //Changes the label of Activity to differ from label of application
        this.setTitle("Login");

        //Checks if token is saved
        if (ApiClient.LoadToken(this)) {
            // if token is  saved opens new screen blog list screen
            Intent intent = new Intent(this, BlogListActivity.class);
            this.startActivity(intent);
            finish();
            return;
        }



        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                //Returns true if the internet is turned on and false if its not
                if (!isNetworkAvailable()) {
                    Toast.makeText(MainActivity.this, "Turn your Internet connection on!", Toast.LENGTH_LONG).show();
                    return;
                }
                //Checks if input email is correct
                if (!validateEmail(email.getText().toString())) {
                    email.setError("Invalid Email!");
                    email.requestFocus();
                    return;
                }
                //Checks if input password is correct
                if (!validatePassword(password.getText().toString())) {
                    password.setError("Invalid Password!");
                    password.requestFocus();
                    return;
                }
                //Checks if the credentials are validated.If yes the request is sent to server
                final Map<String, String> headers = new HashMap<>();
                headers.put("X-Client-Platform", "Android");
                headers.put("Accept", "application/json");
                headers.put("Content-Type", "application/json");

                //Instantiates new object of Data
                Data obj = new Data(email.getText().toString(), password.getText().toString());

                try {
                    //Sends a call to a server
                    CreitiveAPI api = ApiClient.getClient().create(CreitiveAPI.class);
                    Call<Token> call = api.login(headers, obj);
                    call.enqueue(new Callback<Token>() {
                        @Override
                        public void onResponse(Call<Token> call, Response<Token> response) {
                            switch (response.code()) {
                                case 200:
                                    //Saves token in SharedPreferences if the response is ok
                                    ApiClient.SaveToken(response.body(), MainActivity.this);
                                    Intent intent = new Intent(MainActivity.this, BlogListActivity.class);
                                    MainActivity.this.startActivity(intent);
                                    finish();
                                    break;
                                case 400:;
                                    Toast.makeText(MainActivity.this, "Please check your username and password and try again!", Toast.LENGTH_LONG).show();
                                    break;
                                case 401:
                                    Toast.makeText(MainActivity.this, "Wrong email or password!", Toast.LENGTH_LONG).show();
                                    break;
                                case 406:
                                    Toast.makeText(MainActivity.this, "System error - no content type!", Toast.LENGTH_LONG).show();
                                    break;
                                case 415:
                                    Toast.makeText(MainActivity.this, "System error!", Toast.LENGTH_LONG).show();
                                    break;
                            }
                        }

                        @Override
                        public void onFailure(Call<Token> call, Throwable t) {
                            String s = t.getMessage().toString();
                            Toast.makeText(MainActivity.this, "System error-Failed to get token!", Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
    }
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    //Returns true if email is valid and false if email is invalid
     public boolean validateEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    //Returns true if password is valid and false if password in invalid
     public boolean validatePassword(String password) {
        if (password != null && password.length() > 6) {
            return true;
        } else {
            return false;
        }
    }
    //Checks if the internet is turned on
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
