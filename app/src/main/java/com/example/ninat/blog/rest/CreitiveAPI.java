/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.ninat.blog.rest;

import com.example.ninat.blog.model.Blog;
import com.example.ninat.blog.model.Data;
import com.example.ninat.blog.model.BlogContent;
import com.example.ninat.blog.model.Token;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Path;


/**
 * Created by ninat on 23/03/2017.
 *
 */

public interface CreitiveAPI {
    @POST("login")
    Call<Token> login(@HeaderMap Map<String, String> headers, @Body Data request);

    @GET("blogs")
    Call<List<Blog>> getPosts(@HeaderMap Map<String, String> headers);

    @GET("blogs/{id}")
    Call<BlogContent> getSelectedBlog(@Path("id")int id, @HeaderMap Map<String, String> headers);
}






