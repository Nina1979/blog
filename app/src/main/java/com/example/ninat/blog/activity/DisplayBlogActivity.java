/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.ninat.blog.activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ninat.blog.R;
import com.example.ninat.blog.model.BlogContent;
import com.example.ninat.blog.model.Token;
import com.example.ninat.blog.rest.ApiClient;
import com.example.ninat.blog.rest.CreitiveAPI;
import com.orm.SugarRecord;
import com.orm.util.NamingHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ninat on 26/03/2017.
 * Displays single blog on the screen
 */

public class DisplayBlogActivity extends AppCompatActivity {
    public static final String MIME_TYPE = "text/html";
    public static final String ENCODING = "UTF-8";
    @BindView(R.id.wvSelectedBlog) WebView mContentBlog;


    public DisplayBlogActivity() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_blog);
        ButterKnife.bind(this);

        //Takes message from intent by the key
        Bundle extras = getIntent().getExtras();
        Integer blogID = extras.getInt("blogId");


        //Checks if there is internet connection on
        if (!isNetworkAvailable()) {
            Toast.makeText(this, "Turn your Internet connection on!", Toast.LENGTH_LONG).show();
        }
        //Takes list of blogs from database which have this blogID
        List<BlogContent> blogs = SugarRecord.find(BlogContent.class, NamingHelper.toSQLNameDefault("blogId") + " = ?", String.valueOf(blogID));
        // Checks if list is empty.If it is empty fetches new data.If its not empty, takes a blog and its content and sets it on webview
        if (blogs.isEmpty()) {
            fetchData(blogID);
        } else {
            mContentBlog.loadData(blogs.get(0).getContent(), MIME_TYPE, ENCODING);
        }
    }

    //Sends new request to server in order to get single blog
    private void fetchData(Integer blogID) {
        Token mToken = ApiClient.token;

        //Creates an instance of HashMap for  saving a dynamic header
        Map<String, String> mMap = new HashMap<>();
        mMap.put("X-Client-Platform", "Android");
        mMap.put("Accept", "application/json");
        mMap.put("Content-Type", "application/json");
        mMap.put("X-Authorize", mToken.getsToken());

        //Sends a call to a server
        CreitiveAPI api = ApiClient.getClient().create(CreitiveAPI.class);
        Call<BlogContent> call = api.getSelectedBlog(blogID, mMap);
        call.enqueue(new Callback<BlogContent>() {
            @Override
            public void onResponse(Call<BlogContent> call, Response<BlogContent> response) {
                switch (response.code()) {
                    case 200:
                        //Takes a single blog from response, saves it to database and sets it on web view
                        BlogContent mBlog = response.body();
                        SugarRecord.save(mBlog);
                        mContentBlog.loadData(String.valueOf(mBlog.getContent()), MIME_TYPE, ENCODING);
                        break;
                    case 401:
                        Toast.makeText(DisplayBlogActivity.this, "Token is missing!", Toast.LENGTH_LONG).show();
                        break;
                    case 406:
                        Toast.makeText(DisplayBlogActivity.this, "System error!", Toast.LENGTH_LONG).show();
                        break;
                    case 415:
                        Toast.makeText(DisplayBlogActivity.this, "System error!", Toast.LENGTH_LONG).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<BlogContent> call, Throwable t) {
                Toast.makeText(DisplayBlogActivity.this, "System error!", Toast.LENGTH_LONG).show();
            }
        });
    }

    //Checks if the internet is turned on
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}




