/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.ninat.blog.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ninat.blog.R;
import com.example.ninat.blog.activity.DisplayBlogActivity;
import com.example.ninat.blog.model.Blog;

import java.io.InputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ninat on 25/03/2017.
 *
 */

public class ListAdapter extends ArrayAdapter {
    private Context mContext;
    List<Blog> mBlogs;


    public ListAdapter(Context context, List<Blog> blogs) {

        super(context,0,blogs);
        this.mBlogs = blogs;
        this.mContext = context;
    }


    static class ViewHolder {
        @BindView(R.id.txtTitle) TextView txtTitle;
        @BindView(R.id.image) ImageView image;
        @BindView(R.id.txtDescription) TextView txtDescription;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ViewHolder viewHolder;
        final Blog item = (Blog) getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Populate the data into the template view using the data object
        viewHolder.txtTitle.setText(item.getTitle());

        viewHolder.txtTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifyDataSetChanged();
                Intent intent = new Intent(mContext, DisplayBlogActivity.class);
                //Checks if blog exists. If it exists it takes its id and puts it into intent message
                if (item != null) {
                    Integer mId = item.getBlogId();
                    intent.putExtra("blogId", mId);
                    mContext.startActivity(intent);

                }
            }
        });

                new DownloadImageTask(viewHolder.image).execute(item.getImageURL());
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
           viewHolder.txtDescription.setText(Html.fromHtml(item.getDescription(),Html.FROM_HTML_MODE_COMPACT));
      }
        else  viewHolder.txtDescription.setText(Html.fromHtml(item.getDescription()));

        // Return sthe completed view to render on screen
        return convertView;

    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView mImageView;

        public DownloadImageTask(ImageView imageView) {
            this.mImageView = imageView;
        }

        protected Bitmap doInBackground(String... urls) {
            String mUrlDisplay = urls[0];
            Bitmap mBitmap = null;
            try {
                InputStream mInput = new java.net.URL(mUrlDisplay).openStream();
                mBitmap = BitmapFactory.decodeStream(mInput);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mBitmap;
        }

        protected void onPostExecute(Bitmap result) {
            mImageView.setImageBitmap(result);
        }
    }
}
