/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.ninat.blog.rest;

import android.content.Context;
import android.preference.PreferenceManager;

import com.example.ninat.blog.model.Token;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by ninat on 23/03/2017.
 * Creates, retrofit instance, saves token and loads token
 */

public class ApiClient {
    public static final String BASE_URL = "http://blogsdemo.creitiveapps.com/";
    public static Retrofit retrofit = null;
    public static Token token;

    public static Retrofit getClient() {
        if (retrofit == null) {
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static void SaveToken(Token token, Context context) {
        //saves token
        ApiClient.token = token;
        //makes a gson instance
        Gson gson = new Gson();
        //creates a json string
        String json = gson.toJson(token);
        //Saves token into shared preferences
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("Token", json).apply();
    }

    public static boolean LoadToken(Context context) {
        //Takes json string from shared preferences
        String jsonString = PreferenceManager.getDefaultSharedPreferences(context).getString("Token", null);
        if (jsonString == null) {
            return false;
        }
        //creates a new gson instance
        Gson gson = new Gson();
        //Creates token java object from json string
        ApiClient.token = gson.fromJson(jsonString, Token.class);
        return true;
    }
}

